
WITH t AS(
SELECT archeologist_id, COUNT(subjects.name) AS c
FROM findings JOIN subjects ON findings.subject_id = subjects.id
WHERE era = 'middle age' AND date >= '2012-01-01' AND date <= '2012-12-31'
GROUP by archeologist_id )
SELECT name, qualification, c
FROM archeologist JOIN t ON archeologist.id = t.archeologist_id