
WITH t2 AS (
SELECT id, subjects.name AS name, t1.name AS own, era, cost
FROM subjects JOIN 
(
SELECT sub_id, name 
FROM own_sub JOIN owner ON own_sub.own_id = owner.id 
) AS t1 ON t1.sub_id = subjects.id
WHERE era = 'ancient Egipt'
)
SELECT DISTINCT id, name, own, era, cost
FROM t2 JOIN findings ON findings.subject_id = t2.id
WHERE place = 'Kairo'
