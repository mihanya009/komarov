CREATE VIEW view_1 AS
WITH t AS (
SELECT archeologist.id AS id, name AS arch_name, c
FROM archeologist LEFT JOIN 
( 
SELECT archeologist_id AS id, COUNT(name) AS c
FROM findings JOIN subjects ON subject_id = id
WHERE cost > 1000
GROUP BY archeologist_id
) AS t ON archeologist.id = t.id
)
SELECT name, COUNT(tab.spec_id) AS count_arch, SUM(tab.c) AS sum_find
FROM specialization JOIN 
(
SELECT spec_id, arch_name, c
FROM t JOIN arch_spec ON t.id = arch_spec.arch_id
) AS tab ON specialization.id = tab.spec_id
GROUP BY name