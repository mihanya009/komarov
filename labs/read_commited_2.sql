SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRANSACTION B

SELECT * FROM archeologist
WAITFOR DELAY '00:00:10'
UPDATE archeologist SET salary = salary + 50 
WAITFOR DELAY '00:00:10'
DELETE FROM archeologist WHERE id = 2
WAITFOR DELAY '00:00:10'
UPDATE archeologist SET salary = salary + 50 WHERE id = 2
WAITFOR DELAY '00:00:10'
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (21, 'Michael Mosh', 800, 'high')
WAITFOR DELAY '00:00:10'

ROLLBACK TRANSACTION B