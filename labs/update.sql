UPDATE archeologist
SET salary = (CASE 
WHEN qualification = 'high' THEN salary + 10
WHEN qualification = 'low' THEN salary - 10
END)
WHERE qualification = 'low' OR qualification = 'high'
