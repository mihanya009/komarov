/* row_number */

WITH t AS (
SELECT findings.subject_id AS sub_id, COUNT(name) as arch_count
FROM findings JOIN archeologist ON findings.archeologist_id = archeologist.id
GROUP BY findings.subject_id )
SELECT ROW_NUMBER() OVER(ORDER BY subjects.name) num, name, arch_count
FROM subjects JOIN t ON subjects.id = t.sub_id