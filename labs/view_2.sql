CREATE VIEW view_2 AS
SELECT place, date, COUNT(archeologist.name) AS count_arch, COUNT(subjects.name) AS count_sub
FROM (findings JOIN archeologist ON findings.archeologist_id = archeologist.id) JOIN subjects ON findings.subject_id = subjects.id
GROUP BY place, date