/*�������� � ��������, ����������:
id ��������, ��� ���������, ���������� �������*/

SELECT * INTO subjects_large_2 FROM (
SELECT t.id AS id, name,
CASE 
WHEN sub_count IS NULL
THEN 0
ELSE sub_count 
END sub_count
FROM archeologist LEFT JOIN (
SELECT archeologist_id AS id, COUNT(name) AS sub_count
FROM findings JOIN subjects ON findings.subject_id = subjects.id
GROUP BY archeologist_id )
t ON archeologist.id = t.id
) t2

SELECT * INTO subjects_large_index_2 FROM  (
SELECT t.id AS id, name,
CASE 
WHEN sub_count IS NULL
THEN 0
ELSE sub_count 
END sub_count
FROM archeologist LEFT JOIN (
SELECT archeologist_id AS id, COUNT(name) AS sub_count
FROM findings JOIN subjects ON findings.subject_id = subjects.id
GROUP BY archeologist_id )
t ON archeologist.id = t.id
) t2

GO 

INSERT INTO subjects_large_2
SELECT name, sub_count FROM (
SELECT t.id AS id, name,
CASE 
WHEN sub_count IS NULL
THEN 0
ELSE sub_count 
END sub_count
FROM archeologist LEFT JOIN (
SELECT archeologist_id AS id, COUNT(name) AS sub_count
FROM findings JOIN subjects ON findings.subject_id = subjects.id
GROUP BY archeologist_id )
t ON archeologist.id = t.id
) t2


INSERT INTO subjects_large_index_2
SELECT name, sub_count FROM (
SELECT t.id AS id, name,
CASE 
WHEN sub_count IS NULL
THEN 0
ELSE sub_count 
END sub_count
FROM archeologist LEFT JOIN (
SELECT archeologist_id AS id, COUNT(name) AS sub_count
FROM findings JOIN subjects ON findings.subject_id = subjects.id
GROUP BY archeologist_id )
t ON archeologist.id = t.id
) t2

GO 100000

SELECT name 
FROM subjects_large_2
WHERE sub_count > 1

SET STATISTICS TIME ON
SET STATISTICS IO ON

GO

CHECKPOINT
GO
DBCC DROPCLEANBUFFERS
GO

CREATE NONCLUSTERED INDEX IX_name_2 ON dbo.subjects_large_index_2
(
sub_count ASC
)


SELECT name 
FROM subjects_large_2
WHERE sub_count > 1

SELECT name 
FROM subjects_large_index_2
WHERE sub_count > 1


