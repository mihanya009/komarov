/*��� ������� 10 ��������� ���������� �������� ��� ���������� ������ 1000$
���������� �������� ��������� � 1.4 ���� � ���������� ������������*/

CREATE TRIGGER trig
ON archeologist AFTER UPDATE AS
IF 10 < ( SELECT sub_count FROM (
SELECT t.id AS arch_id, name, 
CASE 
WHEN sub_count IS NULL
THEN 0
ELSE sub_count 
END sub_count
FROM archeologist LEFT JOIN (
SELECT archeologist_id AS id, COUNT(name) AS sub_count
FROM findings JOIN subjects ON findings.subject_id = subjects.id
GROUP BY archeologist_id )
t ON archeologist.id = t.id) t2
WHERE arch_id = (
SELECT id FROM inserted)
)

BEGIN

UPDATE archeologist
SET salary = salary * 1.4
WHERE id = (SELECT id FROM inserted)

UPDATE archeologist
SET qualification = 
CASE 
WHEN qualification = 'low'
THEN 'middle'
WHEN qualification = 'middle'
THEN 'high'
WHEN qualification = 'high'
THEN 'high'
END
WHERE id = (SELECT id FROM inserted)

END