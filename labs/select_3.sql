WITH t AS (
SELECT archeologist_id, cost
FROM findings JOIN subjects ON findings.subject_id = subjects.id
)
SELECT name, AVG(cost) AS avg_cost
FROM archeologist LEFT JOIN t ON archeologist.id = t.archeologist_id
GROUP BY name