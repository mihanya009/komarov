/* ������� ������� � ������� ���������*/
WITH t AS (
SELECT archeologist_id AS id, COUNT(name) AS sub_count
FROM findings JOIN subjects ON findings.subject_id = subjects.id
GROUP BY archeologist_id )
SELECT name, 
CASE 
WHEN sub_count IS NULL
THEN 0
ELSE sub_count 
END sub_count
FROM archeologist LEFT JOIN t ON archeologist.id = t.id
