DROP TABLE [own_sub];
DROP TABLE [arch_spec];
DROP TABLE [findings];
DROP TABLE [owner];
DROP TABLE [specialization];
DROP TABLE [archeologist];
DROP TABLE [subjects];

CREATE TABLE [owner]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	CONSTRAINT [PK_owner] PRIMARY KEY ([id] ASC)
);

CREATE TABLE [specialization]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	CONSTRAINT [PK_specialization] PRIMARY KEY ([id] ASC)
);

CREATE TABLE [subjects]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[era] [varchar](50),
	[cost] [int] NOT NULL,
	CONSTRAINT [PK_subjects] PRIMARY KEY ([id] ASC)
);

CREATE TABLE [archeologist]
(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[salary] [int] NOT NULL,
	[qualification] [varchar](50) NOT NULL,
	CONSTRAINT [PK_archeologist] PRIMARY KEY ([id] ASC)
);

CREATE TABLE [own_sub]
(
	[own_id] [int],
	[sub_id] [int] NOT NULL,
);

CREATE TABLE [arch_spec]
(
	[arch_id] [int] NOT NULL,
	[spec_id] [int] NOT NULL,
);

CREATE TABLE [findings]
(
	[archeologist_id] [int] NOT NULL,
	[subject_id] [int] NOT NULL,
	[place] [varchar](50) NOT NULL,
	[date] [date] NOT NULL,
	[type] [varchar](50) NOT NULL,
	[status] [varchar](50) NOT NULL,
);

CREATE UNIQUE INDEX [IX_specialization] ON [specialization]
(
	[name] ASC
);

CREATE UNIQUE INDEX [IX_archeologist] ON [archeologist]
(
	[name] ASC
);

CREATE UNIQUE INDEX [IX_owner] ON [owner]
(
	[name] ASC
);

CREATE UNIQUE INDEX [IX_subjects] ON [subjects]
(
	[name] ASC
);

ALTER TABLE [findings]
ADD CONSTRAINT [FK_findings_archeologist] FOREIGN KEY ([archeologist_id])
REFERENCES [archeologist] ([id]);

ALTER TABLE [findings]
ADD CONSTRAINT [FK_findings_subgects] FOREIGN KEY ([subject_id])
REFERENCES [subjects] ([id]);

ALTER TABLE [own_sub]
ADD CONSTRAINT [FK_own_sub_subjects] FOREIGN KEY ([sub_id])
REFERENCES [subjects] ([id]);

ALTER TABLE [own_sub]
ADD CONSTRAINT [FK_own_sub_owner] FOREIGN KEY ([own_id])
REFERENCES [owner] ([id]);

ALTER TABLE [arch_spec]
ADD CONSTRAINT [FK_arch_spec_archeologist] FOREIGN KEY ([arch_id])
REFERENCES [archeologist] ([id]);

ALTER TABLE [arch_spec]
ADD CONSTRAINT [FK_arch_spec_specialization] FOREIGN KEY ([spec_id])
REFERENCES [specialization] ([id]);

ALTER TABLE arheologist
ADD CONSTRAINT CK_arheologist_salary
CHECK (salary > 0)

ALTER TABLE findings
ADD CONSTRAINT CK_findings_date
CHECK (date > '2000-01-01')

ALTER TABLE subjects
ADD CONSTRAINT CK_subjects_cost
CHECK (cost > 0)


DELETE FROM [own_sub];
DELETE FROM [arch_spec];
DELETE FROM [findings];
DELETE FROM [owner];
DELETE FROM [specialization];
DELETE FROM [archeologist];
DELETE FROM [subjects];

SET IDENTITY_INSERT [specialization] ON
INSERT [specialization] ([id], [name]) VALUES (1, 'egiptologist')
INSERT [specialization] ([id], [name]) VALUES (2, 'assiriologist')
INSERT [specialization] ([id], [name]) VALUES (3, 'underwater')
INSERT [specialization] ([id], [name]) VALUES (4, 'paleoanthropologist')
INSERT [specialization] ([id], [name]) VALUES (5, 'classic')
INSERT [specialization] ([id], [name]) VALUES (6, 'biblical')
INSERT [specialization] ([id], [name]) VALUES (7, 'industrial')
INSERT [specialization] ([id], [name]) VALUES (8, 'paleontologist')
INSERT [specialization] ([id], [name]) VALUES (9, 'theotetic')
INSERT [specialization] ([id], [name]) VALUES (10, 'ancient russian')
SET IDENTITY_INSERT [specialization] OFF

SET IDENTITY_INSERT [owner] ON
INSERT [owner] ([id], [name]) VALUES (1, 'Ceasar')
INSERT [owner] ([id], [name]) VALUES (2, 'Archimedes')
INSERT [owner] ([id], [name]) VALUES (3, 'Ivan the Terrible')
INSERT [owner] ([id], [name]) VALUES (4, 'Ramses II')
INSERT [owner] ([id], [name]) VALUES (5, 'Cleopatra')
INSERT [owner] ([id], [name]) VALUES (6, 'Kir II')
INSERT [owner] ([id], [name]) VALUES (7, 'Alexander the Great')
INSERT [owner] ([id], [name]) VALUES (8, 'Knyaz Vladimir')
INSERT [owner] ([id], [name]) VALUES (9, 'Christopher Columbus')
INSERT [owner] ([id], [name]) VALUES (10, 'Isaac Newton')
SET IDENTITY_INSERT [owner] OFF

SET IDENTITY_INSERT [archeologist] ON
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (1, 'John Smith', 700, 'high')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (2, 'Kianu Rivs', 600, 'high')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (3, 'George Goodman', 500, 'middle')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (4, 'George Orwel', 450, 'middle')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (5, 'Mark Evans', 300, 'low')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (6, 'Irvis Shmirvis', 300, 'middle')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (7, 'Van Halen', 650, 'high')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (8, 'Jim Hander', 800, 'high')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (9, 'Marty McFly', 500, 'middle')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (10, 'Luke Skywalker', 750, 'high')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (11, 'Chis Rock', 250, 'low')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (12, 'Eddy Murfey', 300, 'low')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (13, 'Bob McSilent', 700, 'high')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (14, 'Jey Chatterbox', 600, 'high')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (15, 'Eva McFalcon', 400, 'low')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (16, 'John Little', 350, 'middle')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (17, 'Bruce Wayne', 600, 'high')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (18, 'Peter Slizerin', 550, 'middle')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (19, 'Kenny McDawel', 400, 'middle')
INSERT [archeologist] ([id], [name], [salary], [qualification]) VALUES (20, 'Danila', 1000, 'high')
SET IDENTITY_INSERT [archeologist] OFF

SET IDENTITY_INSERT [subjects] ON
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (1, 'ancient manuscript 1', 'ancient Greek', 1200)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (2, 'ancient cup', 'ancient Greek', 500)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (3, 'Egiptian manuscript', 'ancient Egipt', 1100)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (4, 'gold coin', 'middle age', 700)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (5, 'scepter 1', 'ancient Egipt', 1150)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (6, 'scepter 2', 'middle age', 900)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (7, 'necklace', 'ancient Egipt', 1200)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (8, 'gold ring', 'middle age', 1200)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (9, 'fragments of clothing', 'Roman', 1200)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (10, 'bones', 'Paleozoic', 300)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (11, 'book', 'middle age', 400)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (12, 'compass', 'middle age', 500)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (13, 'human skull', 'Paleozoic', 300)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (14, 'crock', 'ancient Greek', 400)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (15, 'antique vase', 'Roman', 400)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (16, 'the sword', 'ancient Russia', 300)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (17, 'gold treasure', 'middle age', 2450)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (18, 'ancient manuscript 2', 'Roman', 1050)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (19, 'huge key', 'middle age', 600)
INSERT [subjects] ([id], [name], [era], [cost]) VALUES (20, 'gold crown', 'middle age', 1600)
SET IDENTITY_INSERT [subjects] OFF


INSERT [own_sub] ([own_id], [sub_id]) VALUES (1, 18)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (1, 15)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (1, 9)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (2, 1)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (2, 2)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (3, 4)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (4, 5)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (3, 6)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (4, 3)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (5, 7)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (8, 16)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (9, 12)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (3, 19)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (3, 20)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (5, 3)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (2, 14)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (10, 11)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (3, 8)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (NULL, 17)
INSERT [own_sub] ([own_id], [sub_id]) VALUES (NULL, 10)


INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (1, 1)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (2, 4)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (3, 5)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (4, 6)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (5, 2)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (6, 1)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (7, 9)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (8, 5)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (9, 1)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (10, 5)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (11, 10)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (12, 6)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (13, 1)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (14, 1)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (15, 10)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (16, 9)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (17, 8)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (18, 7)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (19, 6)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (20, 5)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (20, 4)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (20, 3)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (14, 2)
INSERT [arch_spec] ([arch_id], [spec_id]) VALUES (1, 4)


INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (1, 3, 'Kairo', '2011-03-03', 'document', 'unlisted')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (1, 7, 'Kairo', '2012-10-20', 'decoration', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (2, 1, 'Athens', '2012-07-14', 'document', 'unlisted')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (3, 4, 'Kremlin', '2011-05-17', 'money', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (3, 19, 'Kremlin', '2011-05-17', 'key', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (3, 20, 'Kremlin', '2011-05-17', 'decoration', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (6, 7, 'Kairo', '2012-10-20', 'decoration', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (7, 14, 'Athens', '2012-07-15', 'dishes', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (8, 15, 'Rome', '2013-04-08', 'dishes', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (9, 9, 'Rome', '2013-04-08', 'clothes', 'unlisted')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (10, 11, 'London', '2012-06-06', 'document', 'unlisted')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (11, 6, 'Kremlin', '2011-05-15', 'decorate', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (12, 12, 'Amsterdam seaside', '2013-09-03', 'measuring device', 'unlisted')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (13, 18, 'Rome', '2014-03-20', 'document', 'unlisted')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (14, 7, 'Kairo', '2012-10-20', 'decoration', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (15, 2, 'Athens', '2012-07-14', 'dishes', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (16, 6, 'Kremlin', '2011-05-15', 'decorate', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (18, 8, 'Velikiy Novgorod', '2010-05-22', 'decorate', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (19, 10, 'The Urals', '2015-11-05', 'bones', 'unlisted')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (20, 16, 'Vladimir', '2013-08-13', 'weapon', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (20, 13, 'Nothern India', '2014-03-14', 'bones', 'unlisted')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (20, 17, 'Black Sea', '2012-07-19', 'money', 'unlisted')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (13, 5, 'Kairo', '2011-03-03', 'decorate', 'handed over to the museum')
INSERT [findings] ([archeologist_id], [subject_id], [place], [date], [type], [status]) VALUES (14, 5, 'Kairo', '2011-03-03', 'decorate', 'handed over to the museum')
